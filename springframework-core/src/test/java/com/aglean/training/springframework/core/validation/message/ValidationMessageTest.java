package com.aglean.training.springframework.core.validation.message;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;

import com.aglean.training.springframework.core.validation.Address;
import com.aglean.training.springframework.core.validation.AddressValidator;
import com.aglean.training.springframework.core.validation.Person;
import com.aglean.training.springframework.core.validation.PersonValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = ValidationMessageConfiguration.class )
public class ValidationMessageTest {

	@Autowired
	ApplicationContext applicationContext;
	
	@Test
	public void test() {
		PersonValidator personValidator = new PersonValidator(new AddressValidator());
		
		Person person = new Person();
		person.setAge(30);
		Address address = new Address();
		address.setCity("Taipei");
		person.setAddress(address);
		
		BindingResult bindingResult = new BeanPropertyBindingResult(person, "person");
		
		ValidationUtils.invokeValidator(personValidator, person, bindingResult);
		String[] codes = bindingResult.getFieldError().getCodes();
		System.out.println(Arrays.toString(codes));
		
		String[] args = {String.valueOf(person.getAge()), person.getAddress().getCity()};
		String message = applicationContext.getMessage(codes[0], args, null);
		System.out.println(message);
		
		assertEquals("Person whose age is 30 and address is Taipei, his/her name is empty.", message);;
	}

}
