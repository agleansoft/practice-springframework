package com.aglean.training.springframework.core.ioc.profile;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aglean.training.springframework.core.ioc.Message;
import com.aglean.training.springframework.core.ioc.Parser;
import com.aglean.training.springframework.core.ioc.profile.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {Application.class, HemingwayResourceConfigure.class, MarkTwainResourceConfigure.class} )
@ActiveProfiles("hemingway")
public class ApplicationHemingwayTest {

	@Autowired
	ApplicationContext applicationContext;
	
	@Test
	public void testHemingway() {
		Message message = applicationContext.getBean(Message.class);
		Parser parser = applicationContext.getBean(Parser.class);
		String[] result = parser.getResult(message);
		String actual = Arrays.toString(result);
		
		int[] expecte = {1934, 1917};
		String expected = Arrays.toString(expecte);
		
		assertEquals(expected, actual);
	}

}
