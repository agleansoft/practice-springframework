package com.aglean.training.springframework.core.resources;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aglean.training.springframework.core.resources.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = Application.class )
public class ApplicationTest {

	@Autowired
	ApplicationContext applicationContext;
	
	@Test
	public void testClassPathResource() {
		Resource classPathResource = applicationContext.getResource("classpath:william_faulkner_quotation.txt");
		String actual = classPathResource.getDescription();
		String expected = "class path resource [william_faulkner_quotation.txt]";
		
		assertEquals(expected, actual);
	}

	@Test
	public void testFileSystemResource() {
		Resource fileResource = applicationContext.getResource("file:william_faulkner_quotation.txt");
		String actual = fileResource.getDescription();
		//System.out.println(actual);
		String expected = "URL [file:william_faulkner_quotation.txt]";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testURLResource() {
		Resource urlResource = applicationContext.getResource("http://www.google.com");
		String actual = urlResource.getDescription();
		String expected = "URL [http://www.google.com]";
		
		assertEquals(expected, actual);
	}
	
}
