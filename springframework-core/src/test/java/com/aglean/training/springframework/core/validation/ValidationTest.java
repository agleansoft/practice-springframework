package com.aglean.training.springframework.core.validation;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;

public class ValidationTest {

	@Test
	public void testNameEmpty() {
		PersonValidator personValidator = new PersonValidator(new AddressValidator());
		
		Person person = new Person();
		person.setAge(30);
		Address address = new Address();
		address.setCity("Taipei");
		person.setAddress(address);
		
		BindingResult bindingResult = new BeanPropertyBindingResult(person, "testPerson");
		
		ValidationUtils.invokeValidator(personValidator, person, bindingResult);
		
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("name", bindingResult.getFieldError().getField());
		assertEquals("empty", bindingResult.getFieldError().getCode());
	}
	
	@Test
	public void testAgeWrong() {
		PersonValidator personValidator = new PersonValidator(new AddressValidator());
		
		Person person = new Person();
		person.setName("Tom");
		person.setAge(-1);
		Address address = new Address();
		address.setCity("Taipei");
		person.setAddress(address);
		
		BindingResult bindingResult = new BeanPropertyBindingResult(person, "testPerson");
		
		ValidationUtils.invokeValidator(personValidator, person, bindingResult);
		
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("age", bindingResult.getFieldError().getField());
		assertEquals("negativeValue", bindingResult.getFieldError().getCode());
		
		person.setAge(111);
		
		BindingResult _bindingResult = new BeanPropertyBindingResult(person, "testPerson");
		
		ValidationUtils.invokeValidator(personValidator, person, _bindingResult);
		
		assertEquals(1, _bindingResult.getErrorCount());
		assertEquals("age", _bindingResult.getFieldError().getField());
		assertEquals("tooOld", _bindingResult.getFieldError().getCode());
	}
	
	@Test
	public void testAddressEmpty() {
		PersonValidator personValidator = new PersonValidator(new AddressValidator());
		
		Person person = new Person();
		person.setName("Tom");
		person.setAge(30);
		
		BindingResult bindingResult = new BeanPropertyBindingResult(person, "testPerson");
		
		ValidationUtils.invokeValidator(personValidator, person, bindingResult);
		
		assertEquals(1, bindingResult.getErrorCount());
		assertEquals("address.city", bindingResult.getFieldError().getField());
		assertEquals("empty", bindingResult.getFieldError().getCode());
	}
}
