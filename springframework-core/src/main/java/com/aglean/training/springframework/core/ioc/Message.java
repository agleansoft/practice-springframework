package com.aglean.training.springframework.core.ioc;

public interface Message {
	public String getText();
}
