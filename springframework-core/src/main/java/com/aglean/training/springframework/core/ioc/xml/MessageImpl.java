package com.aglean.training.springframework.core.ioc.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.springframework.core.io.Resource;

import com.aglean.training.springframework.core.ioc.Message;

public class MessageImpl implements Message {

	private Resource resource;
	
	public MessageImpl(Resource resource) {
		this.resource = resource;
	}
	
	@Override
	public String getText() {
		String text = null;
		InputStream is = null;
		try {			
			is = resource.getInputStream();
			text = new Scanner(is).useDelimiter("\\A").next();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}
	
}
