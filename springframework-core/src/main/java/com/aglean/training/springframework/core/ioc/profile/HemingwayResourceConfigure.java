package com.aglean.training.springframework.core.ioc.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@Profile("hemingway")
public class HemingwayResourceConfigure {

	@Bean
	public Resource setResource() {
		Resource resource = new ClassPathResource("boron/springframework/core/ioc/ernest_hemingway_quotation.txt");
		return resource;
	}
}
