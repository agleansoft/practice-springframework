package com.aglean.training.springframework.core.resources;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("boron.springframework.core.resources")
public class Application {

	/*
	 * Initialize the application context and load different kind of resource into it
	 */
	public static void main(String[] args) {
		// see the test class
	}
}