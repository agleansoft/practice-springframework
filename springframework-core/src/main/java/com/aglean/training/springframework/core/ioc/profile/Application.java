package com.aglean.training.springframework.core.ioc.profile;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("boron.springframework.core.ioc.profile")
public class Application {

	/*
	 * Initialize the application context and parsing the result
	 */
	public static void main(String[] args) {
		// see the test class
	}
}