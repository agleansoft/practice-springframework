package com.aglean.training.springframework.core;

public interface MessageService {
	String getMessage();
}
