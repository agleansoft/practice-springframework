package com.aglean.training.springframework.core.ioc.profile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.aglean.training.springframework.core.ioc.Message;

@Component
public class MessageImpl implements Message {

	private Resource resource;
	
	@Autowired
	public MessageImpl(Resource resource) {
		this.resource = resource;
	}
	
	@Override
	public String getText() {
		String text = null;
		InputStream is = null;
		try {			
			is = resource.getInputStream();
			text = new Scanner(is).useDelimiter("\\A").next();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}
	
}
