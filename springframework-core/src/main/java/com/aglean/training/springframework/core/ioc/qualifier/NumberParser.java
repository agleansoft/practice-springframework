package com.aglean.training.springframework.core.ioc.qualifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.aglean.training.springframework.core.ioc.Message;
import com.aglean.training.springframework.core.ioc.Parser;

@Component
public class NumberParser implements Parser {

	@Override
	public String[] getResult(Message message) {
		Pattern pattern = Pattern.compile("(\\s+)[0-9]+");
		Matcher matcher = pattern.matcher(message.getText());
		
		StringBuilder sb = new StringBuilder();
		while (matcher.find()) {
			sb.append(message.getText().substring(matcher.start(), matcher.end()));
		}
		
		return sb.toString().trim().split("\\s+");
	}

}
