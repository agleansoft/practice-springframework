package com.aglean.training.springframework.core.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class PersonValidator implements Validator {

	private Validator addressValidator;

	public PersonValidator(Validator addressValidator) {
		if (addressValidator == null) {
			throw new IllegalArgumentException("The supplied [Validator] is " + "required and must not be null.");
		}
		if (!addressValidator.supports(Address.class)) {
			throw new IllegalArgumentException("The supplied [Validator] must " + "support the validation of [Address] instances.");
		}
		this.addressValidator = addressValidator;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return Person.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "name", "empty");
		Person person = (Person) target;
		if (person.getAge() <= 0) {
			errors.rejectValue("age", "negativeValue");
		} else if (person.getAge() > 110) {
			errors.rejectValue("age", "tooOld");
		}
		
		try {
			errors.pushNestedPath("address");
			ValidationUtils.invokeValidator(addressValidator, person.getAddress(), errors);
		} finally {
			errors.popNestedPath();
		}
		
	}

}
