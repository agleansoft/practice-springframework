package com.aglean.training.springframework.core.ioc;

import com.aglean.training.springframework.core.ioc.Message;

public interface Parser {
	public String[] getResult(Message message);
}
