package com.aglean.training.springframework.core.ioc.qualifier;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@ComponentScan("boron.springframework.core.ioc.qualifier")
public class Application {

	@Bean
	@Qualifier("twain")
	public Resource setResourceOne() {
		Resource resource = new ClassPathResource("boron/springframework/core/ioc/mark_twain_quotation.txt");
		return resource;
	}
	
	@Bean
	@Qualifier("hemingway")
	public Resource setResourceTwo() {
		Resource resource = new ClassPathResource("boron/springframework/core/ioc/ernest_hemingway_quotation.txt");
		return resource;
	}

	/*
	 * Initialize the application context and parsing the result
	 */
	public static void main(String[] args) {
		// see the test class
	}
}