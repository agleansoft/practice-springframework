package com.aglean.training.springframework.core.ioc.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@ComponentScan("boron.springframework.core.ioc.annotation")
public class Application {

	@Bean
	public Resource setResource() {
		Resource resource = new ClassPathResource("boron/springframework/core/ioc/mark_twain_quotation.txt");
		return resource;
	}

	/*
	 * Initialize the application context and parsing the result
	 */
	public static void main(String[] args) {
		// see the test class
	}
}